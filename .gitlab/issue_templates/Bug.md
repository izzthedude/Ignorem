## Description

Generally describe this bug in one concise sentence.

Then, elaborate on this bug in detail.

### Subtitle 1

Use subtitles if needed to better organise this bug description.

## Steps to reproduce

Describe how this bug can be recreated.

1. Step 1
2. Step 2
3. Step 3
4. etc etc

## Current vs Expected behaviour

Describe the current behaviour (the bug).

Then, describe the expected behaviour.

## Relevant logs

<details>
<summary>Logs</summary>

```
Put your logs in this block
```

</details>

<details>
<summary>Screenshots (delete this section if no screenshots)</summary>

#### Image 1

<!-- Image 1 link here -->

#### Image 2

<!-- Image 2 link here -->

</details>

## Possible fixes

If you know where the source of the problem is in the code, state the file(s) and line(s).

If you have any suggestions for potential solutions, describe it here.

You can remove either if one of them don't apply to you. If neither applies to you,
then you can delete this entire section.

/label ~"type::bug"
