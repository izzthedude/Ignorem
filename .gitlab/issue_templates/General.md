## Description

Generally describe this issue in one concise sentence.

Then, elaborate on this issue in detail. This is for issues not related to the
app itself. This includes:

- Bad/lacking documentation
- Code problems (bad practices, etc.)
- Project identity/metadata problems

#### Subtitle

Use subtitles if needed to better organise this issue description.

## Possible fixes

If you have any suggestions for potential solutions, describe it here.

/label ~"type::bug"
